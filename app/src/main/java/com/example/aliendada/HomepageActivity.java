package com.example.aliendada;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HomepageActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvPincodeValidation,tvresult;
    EditText etPincode;
    Button btnValidation;
    JsonArrayRequest objectRequest;

    String pinCode;
    public RequestQueue requestQueue;
    JSONObject obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        tvPincodeValidation = findViewById(R.id.tvPincodeValidation);
        etPincode = findViewById(R.id.etPincode);
        btnValidation = findViewById(R.id.btnValidate);
        tvresult = findViewById(R.id.tvresult);

        requestQueue = Volley.newRequestQueue(this);

        btnValidation.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        pinCode = etPincode.getText().toString();

        if (TextUtils.isEmpty(pinCode)){
            Toast.makeText(this, "Please enter valid pincode", Toast.LENGTH_SHORT).show();
        }
        else {
            getDataFromPinCode(pinCode);
        }
    }

    private void getDataFromPinCode(String pinCode) {

        // clearing our cache of request queue.
        requestQueue.getCache().clear();

        // below is the url from where we will be getting
        // our response in the json format.
        String url = "https://api.postalpincode.in/pincode/" + pinCode;

        // below line is use to initialize our request queue.
        RequestQueue queue = Volley.newRequestQueue(this);

        // in below line we are creating a
        // object request using volley.
         objectRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
             @Override
             public void onResponse(JSONArray response) {

                 for (int i=0;i<response.length();i++){
                    try{
                        JSONObject jsonObject = response.getJSONObject(i);
                        JSONArray array = jsonObject.getJSONArray("PostOffice");
                        for (int j=0;j< array.length();j++){
                            JSONObject obj = array.getJSONObject(j);
                            String District = obj.getString("District");
                            String State = obj.getString("State");
                            String Country = obj.getString("Country");
                            tvresult.setText("District :"+District+"\n"
                            +"State :"+State+"\n"+
                                    "Country :"+Country);
                        }

                    }
                    catch (JSONException j){
                        j.printStackTrace();
                        Toast.makeText(HomepageActivity.this, j.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                 }

             }
         },
                 new Response.ErrorListener() {
                     @Override
                     public void onErrorResponse(VolleyError error) {
                         Toast.makeText(HomepageActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                     }
                 });
         queue.add(objectRequest);
    }
}


